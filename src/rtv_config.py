#!/usr/bin/python

import os, time

# TODO: put these in a config file, obviously

cfg_config_dir = "~/.recordtv"
cfg_xmltv_command = "tv_grab_uk_rt"

# -------------

class GTVGConfig:

	def __init__( self, options, args ):
		self.options = options
		self.args = args

		self.extra_recording_time_mins = 5

		self.config_dir = os.path.expanduser( cfg_config_dir )

		self.xmltv_configs_dir = os.path.join( self.config_dir,
			"xmltv_configs" )

		self.xmltv_files_dir = os.path.join( self.config_dir,
			"xmltv_files" )

		self.xmltv_config_file = os.path.join( self.xmltv_configs_dir,
			cfg_xmltv_command + ".conf" )

		self.xmltv_download_command = ( cfg_xmltv_command, "--quiet",
			"--config-file", self.xmltv_config_file )

		self.xmltv_config_command = ( cfg_xmltv_command, "--quiet",
			"--configure", "--gui",
			"--config-file", self.xmltv_config_file )

		self.recorded_progs_dir = os.path.join( self.config_dir,
			"recorded_programmes" )

		self.tvguide_dir = os.path.join( self.recorded_progs_dir, "tvguide" )

		self.scheduled_events_dir = os.path.join( self.config_dir,
			"scheduled_events" )

		self.recording_log_dir = os.path.join( self.config_dir,
			"recording_log" )

		self.favourites_dir = os.path.join( self.config_dir,"favourites" )
		self.selections_dir = os.path.join( self.config_dir,"selections" )
		self.config_config_dir = os.path.join( self.config_dir, "config" )

		self.channel_xmltv2tzap_file = os.path.join( self.config_config_dir,
			"xmltv_channel_to_tzap.rtvcfg" )

		self.channels_order_file = os.path.join( self.config_config_dir,
			"channels_order.rtvcfg" )

		self.html_templates_dir = os.path.join( self.config_dir,
			"html_templates" )

		self.html_single_day_file = os.path.join( self.html_templates_dir,
			"doc_single_day.html" )
		self.html_programme_small_normal_file = os.path.join(
			self.html_templates_dir, "div_programme_normal.html" )
		self.html_programme_small_highlight_file = os.path.join(
			self.html_templates_dir, "div_programme_highlight.html" )
		self.html_programme_small_remind_file = os.path.join(
			self.html_templates_dir, "div_programme_remind.html" )
		self.html_programme_small_record_file = os.path.join(
			self.html_templates_dir, "div_programme_record.html" )

		self.html_programme_full_normal_file = os.path.join(
			self.html_templates_dir, "full_programme_normal.html" )
		self.html_programme_full_highlight_file = os.path.join(
			self.html_templates_dir, "full_programme_highlight.html" )
		self.html_programme_full_remind_file = os.path.join(
			self.html_templates_dir, "full_programme_remind.html" )
		self.html_programme_full_record_file = os.path.join(
			self.html_templates_dir, "full_programme_record.html" )
		self.html_time_file = os.path.join(
			self.html_templates_dir, "div_time.html" )

	def get_listings_filename( self, struct_time ):
		return os.path.join( self.xmltv_files_dir,
			time.strftime( "tv-%Y-%m-%d.xmltv", struct_time ) )

