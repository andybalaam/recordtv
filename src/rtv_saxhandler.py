#!/usr/bin/python

import rtv_programmeinfo, rtv_utils

XMLTV_DATETIME_FORMAT = "%Y%m%d%H%M%S"

class SaxHandler:

	def __init__( self, callback ):
		self.chars = ""
		self.callback = callback
		self.title = None
		self.sub_title = None
		self.desc = ""
		self.channel = None
		self.startTime = None
		self.endTime = None

	def parse_time( self, xmltv_time ):
		# TODO: handle XMLTV time zones
		return rtv_utils.parse_datetime( xmltv_time[:14],
			XMLTV_DATETIME_FORMAT )

	def startElement( self, name, attrs ):
		if name == "programme":
			if "start" in attrs:
				self.startTime = self.parse_time( attrs['start'] )
			if "stop" in attrs:
				self.endTime = self.parse_time( attrs['stop'] )
			if "channel" in attrs:
				self.channel = attrs['channel']

	def endElement( self, name ):
		self.chars = self.chars.strip()
		if name == "title":
			self.title = self.chars
		if name == "sub-title":
			self.sub_title = self.chars
		if name == "desc":
			self.desc = self.chars
		elif name == "programme" and self.title != None:

			pi = rtv_programmeinfo.ProgrammeInfo()
			pi.title = self.title
			pi.sub_title = self.sub_title
			pi.description = self.desc
			pi.channel = self.channel
			pi.startTime = self.startTime
			pi.endTime = self.endTime
			self.callback( pi )

			self.title = None
			self.sub_title = None
			self.desc = ""
			self.channel = None
			self.startTime = None
			self.endTime = None

		self.chars = ""

	def characters( self, data ):
		self.chars += data

