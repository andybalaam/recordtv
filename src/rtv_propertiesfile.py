#!/usr/bin/python

from rtv_abstractpropertiesfile import AbstractPropertiesFile

class PropertiesFile( AbstractPropertiesFile ):

	def set_value( self, key, value ):
		self.__dict__[key] = value

	def get_value( self, key ):
		return self.__dict__[key]

	def get_keys( self ):
		return self.__dict__.keys()

