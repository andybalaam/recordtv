#!/usr/bin/python

import os, time
import rtv_utils

def delete_old_xmltv_files( config ):

	for fn in os.listdir( config.xmltv_files_dir ):
		if fn[-6:] == ".xmltv":

			full_fn = os.path.join( config.xmltv_files_dir, fn )

			print "Deleting file '%s'" % full_fn

			os.unlink( full_fn )

def download( config ):
	rtv_utils.ensure_dir_exists( config.xmltv_configs_dir )
	rtv_utils.ensure_dir_exists( config.xmltv_files_dir )

	# NOTE: alternatively we could move them to a different dir?
	delete_old_xmltv_files( config )

	if not os.path.isfile( config.xmltv_config_file ):

		if config.options.interactive:
			rtv_utils.run_command( config.xmltv_config_command )

			if not os.path.isfile( config.xmltv_config_file ):
				raise Exception( "Tried to configure XMLTV, but the "
					+ "config file '" + config.xmltv_config_file
					+ "' still does not exist." )
		else:
			raise Exception( "The XMLTV config file '%s' does not exist!"
				% config.xmltv_config_file )

	date_filename = time.strftime( "tv-%Y-%m-%d.xmltv", time.localtime() )
	rtv_utils.run_command_write_output( config.xmltv_download_command,
		os.path.join( config.xmltv_files_dir, date_filename ) )






