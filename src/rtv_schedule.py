#!/usr/bin/python

import time, datetime, os, re
import rtv_favourite, rtv_utils, rtv_programmeinfo
import rtv_propertiesfile, rtv_selection

# TODO: prevent overlapping recordings
# TODO: delete old files:
#          - scheduled recordings
#          - logs
#          - selections

MESSAGE_TIME_FORMAT = "%H:%M on %a"

at_output_re = re.compile( "job (\d+) at .*\n" )
num_re = re.compile( "\d+" )

# TODO: put this in the config file
record_start_command = ( '/home/andy/cvs/recordtv/scripts/record_dvb_to_flv.sh "%s" "%s" "%d" "%s" >> "%s" 2>&1' )

def priority_time_compare( pi1, pi2 ):
	pi1Pri = pi1.get_priority()
	pi2Pri = pi2.get_priority()

	if pi1Pri > pi2Pri:
		return -1
	elif pi1Pri < pi2Pri:
		return 1
	elif pi1.startTime < pi2.startTime:
		return -1
	elif pi1.startTime > pi2.startTime:
		return 1

	return 0

class Schedule:

	def __init__( self, config ):
		self.config = config

	def get_at_job( self, at_output ):
		for ln in at_output:
			m = at_output_re.match( ln )
			if m:
				return m.group( 1 )
		print ( "Unable to understand at command output '%s' - "
			+ "can't create a scheduled_events entry" ) % at_output
		return None

	def print_clash_priority_error( self, losePi, keepPi ):
		print ( ("Not recording '%s' at %s - it clashes with '%s',"
						+ " which is higher priority (%d > %d).")
					% ( losePi.title,
						losePi.startTime.strftime(
							MESSAGE_TIME_FORMAT ),
						keepPi.title, keepPi.get_priority(),
						losePi.get_priority() ) )

	def print_clash_time_error( self, losePi, keepPi ):
		print ( ("Not recording '%s' at %s - it clashes with '%s',"
					+ " which has the same priority (%d), but starts earlier"
					+ " (%s).")
					% ( losePi.title,
						losePi.startTime.strftime(
							MESSAGE_TIME_FORMAT ),
						keepPi.title, keepPi.get_priority(),
						keepPi.startTime.strftime(
							MESSAGE_TIME_FORMAT ) ) )

	def print_clash_same_time( self, losePi, keepPi ):
		print ( ("Not recording '%s' at %s - it clashes with '%s',"
					+ " which has the same priority (%d). They start at"
					+ " the same time, so the one to record was chosen"
					+ " randomly.")
					% ( losePi.title,
						losePi.startTime.strftime(
							MESSAGE_TIME_FORMAT ),
						keepPi.title, keepPi.get_priority() ) )


	def remove_clashes( self ):
		self.record_queue.sort( priority_time_compare )
		new_queue = []

		for pi1Num in range( len( self.record_queue ) ):
			pi1 = self.record_queue[pi1Num]

			clashed_with = None

			for pi2Num in range( pi1Num ):
				pi2 = self.record_queue[pi2Num]

				if pi1.clashes_with( pi2 ):
					clashed_with = pi2
					break

			if not clashed_with:
				new_queue.append( pi1 )
			else:
				if pi1.get_priority() < clashed_with.get_priority():
					self.print_clash_priority_error( pi1, clashed_with )
				elif pi1.startTime > clashed_with.startTime:
					self.print_clash_time_error( pi1, clashed_with )
				else:
					self.print_clash_same_time( pi1, clashed_with )

		self.record_queue = new_queue

	def schedule_recordings( self, queue ):

		rtv_utils.ensure_dir_exists( self.config.recorded_progs_dir )
		rtv_utils.ensure_dir_exists( self.config.scheduled_events_dir )
		rtv_utils.ensure_dir_exists( self.config.recording_log_dir )

		for programmeInfo in queue:

			print "Recording '%s' at '%s'" % ( programmeInfo.title,
				programmeInfo.startTime.strftime( MESSAGE_TIME_FORMAT ) )

			filename = rtv_utils.prepare_filename( programmeInfo.title )
			filename += programmeInfo.startTime.strftime( "-%Y-%m-%d_%H_%M" )

			length_timedelta = programmeInfo.endTime - programmeInfo.startTime
			length_in_seconds = ( ( length_timedelta.days
				* rtv_utils.SECS_IN_DAY ) + length_timedelta.seconds )
			length_in_seconds += 60 * self.config.extra_recording_time_mins

			sched_filename = os.path.join( self.config.scheduled_events_dir,
				filename + ".rtvinfo" )

			outfilename = os.path.join( self.config.recorded_progs_dir,
				filename )
			cmds_array = ( "at",
				programmeInfo.startTime.strftime( "%H:%M %d.%m.%Y" ) )
			at_output = rtv_utils.run_command_feed_input( cmds_array,
				record_start_command % (
					self.channel_xmltv2tzap.get_value( programmeInfo.channel ),
					outfilename, length_in_seconds, sched_filename,
					os.path.join( self.config.recording_log_dir,
						filename + ".log" ) ) )
			at_job_start = self.get_at_job( at_output )

			if at_job_start != None:
				programmeInfo.atJob = at_job_start
				programmeInfo.save( sched_filename )

	def sax_callback( self, pi ):
		for fav in self.favs_and_sels:
			if fav.matches( pi ):
				dtnow = datetime.datetime.today()
				if( pi.startTime > dtnow
						and (pi.startTime - dtnow).days < 1 ):

					if fav.deleteAfterDays:
						pi.deleteTime = pi.endTime + datetime.timedelta(
							float( fav.deleteAfterDays ), 0 )

					pi.channel_pretty = self.channel_xmltv2tzap.get_value(
						pi.channel )
					if not pi.channel_pretty:
						print (
							"Pretty channel name not found for channel %s"
							% pi.channel )
					pi.priority = fav.priority
					self.record_queue.append( pi )
				break

	def remove_scheduled_events( self ):

		rtv_utils.ensure_dir_exists( self.config.scheduled_events_dir )

		for fn in os.listdir( self.config.scheduled_events_dir ):
			full_fn = os.path.join( self.config.scheduled_events_dir, fn )

			pi = rtv_programmeinfo.ProgrammeInfo()
			pi.load( full_fn )

			at_job_start = pi.atJob
			done_atrm = False
			if num_re.match( at_job_start ):
				rtv_utils.run_command( ( "atrm", at_job_start ) )
				done_atrm = True

			if done_atrm:
				os.unlink( full_fn )

	def delete_pending_recordings( self ):
		rtv_utils.ensure_dir_exists( self.config.recorded_progs_dir )

		dttoday = datetime.datetime.today()

		for fn in os.listdir( self.config.recorded_progs_dir ):
			if fn[-4:] == ".flv":

				infofn = os.path.join( self.config.recorded_progs_dir,
					fn[:-4] + ".rtvinfo" )

				if os.path.isfile( infofn ):

					pi = rtv_programmeinfo.ProgrammeInfo()
					pi.load( infofn )

					if pi.deleteTime and pi.deleteTime < dttoday:
						full_fn = os.path.join(
							self.config.recorded_progs_dir, fn )

						print "Deleting file '%s'" % full_fn

						os.unlink( full_fn )
						os.unlink( infofn )



	def schedule( self, xmltv_parser = rtv_utils, fav_reader = rtv_selection,
			scheduler = None ):

		if scheduler == None:
			scheduler = self

		# TODO: if we are generating a TV guide as well as scheduling,
		#       we should share the same XML parser.

		self.delete_pending_recordings()

		self.favs_and_sels = fav_reader.read_favs_and_selections(
			self.config )

		self.channel_xmltv2tzap = rtv_propertiesfile.PropertiesFile()
		self.channel_xmltv2tzap.load( self.config.channel_xmltv2tzap_file )

		scheduler.remove_scheduled_events()

		self.record_queue = []
		xmltv_parser.parse_xmltv_files( self.config, self.sax_callback )
		self.remove_clashes()
		scheduler.schedule_recordings( self.record_queue )
		self.record_queue = []


def schedule( config ):
	sch = Schedule( config )
	sch.schedule()





# === Test code ===

def format_pi_list( qu ):
	ret = "[ "
	for pi in qu[:-1]:
		ret += pi.title
		ret += ", "
	if len( qu ) > 0:
		ret += qu[-1].title
	ret += " ]"

	return ret

class FakeScheduler:

	def __init__( self ):

		self.remove_scheduled_events_called = False

		dtnow = datetime.datetime.today()

		self.favHeroes = rtv_favourite.Favourite()
		self.favHeroes.title_re = "Heroes"

		self.favNewsnight = rtv_favourite.Favourite()
		self.favNewsnight.title_re = "Newsnight.*"
		self.favNewsnight.priority = -50

		self.favLost = rtv_favourite.Favourite()
		self.favLost.title_re = "Lost"
		self.favLost.priority = -50

		self.piHeroes = rtv_programmeinfo.ProgrammeInfo()
		self.piHeroes.title = "Heroes"
		self.piHeroes.startTime = dtnow + datetime.timedelta( hours = 1 )
		self.piHeroes.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piHeroes.channel = "south-east.bbc2.bbc.co.uk"

		self.piNewsnight = rtv_programmeinfo.ProgrammeInfo()
		self.piNewsnight.title = "Newsnight"
		self.piNewsnight.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 30 )
		self.piNewsnight.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piNewsnight.channel = "south-east.bbc1.bbc.co.uk"

		self.piNR = rtv_programmeinfo.ProgrammeInfo()
		self.piNR.title = "Newsnight Review"
		self.piNR.startTime = dtnow + datetime.timedelta( hours = 2 )
		self.piNR.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piNR.channel = "south-east.bbc2.bbc.co.uk"

		self.piLost = rtv_programmeinfo.ProgrammeInfo()
		self.piLost.title = "Lost"
		self.piLost.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 25 )
		self.piLost.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piLost.channel = "channel4.com"

		self.piTurnip = rtv_programmeinfo.ProgrammeInfo()
		self.piTurnip.title = "Newsnight Turnip"
		self.piTurnip.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 30 )
		self.piTurnip.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piTurnip.channel = "channel5.co.uk"

		self.which_test = None

	def parse_xmltv_files( self, config, callback ):
		if self.which_test == "no_clash1":
			callback( self.piNewsnight )
			callback( self.piNR )
		elif self.which_test == "priority_clash1":
			callback( self.piHeroes )
			callback( self.piNewsnight )
		elif self.which_test == "time_clash1":
			callback( self.piNewsnight )
			callback( self.piLost )
		elif self.which_test == "same_clash1":
			callback( self.piNewsnight )
			callback( self.piTurnip )

	def remove_scheduled_events( self ):
		if self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events called twice." )
		self.remove_scheduled_events_called = True

	def read_favs_and_selections( self, config ):
		return [ self.favHeroes, self.favNewsnight, self.favLost ]

	def schedule_recordings( self, queue ):
		self.queue = queue

	def test( self ):
		self.which_test = "priority_clash1"
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piHeroes]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piHeroes], self.queue ) )


		self.which_test = "no_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piNewsnight, self.piNR]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piNewsnight, self.piNR], self.queue ) )

		self.which_test = "time_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piLost]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piLost], self.queue ) )

		self.which_test = "same_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piNewsnight] and self.queue != [self.piTurnip]:
			raise Exception( ("queue should look like %s or %s, but it"
					+ " looks like %s" )
				% ( format_pi_list( [self.piNewsnight] ),
					format_pi_list( [self.piTurnip] ),
					format_pi_list( self.queue ) ) )

		print "test_schedule Passed"

def test_schedule( config ):
	p = FakeScheduler()
	p.schedule = Schedule( config )

	p.test()


