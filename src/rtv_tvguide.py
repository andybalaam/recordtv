#!/usr/bin/python

import datetime, os
import rtv_config, rtv_propertiesfile, rtv_utils, rtv_selection
from rtv_orderedpropertiesfile import OrderedPropertiesFile

FILENAME_DATE_FORMAT = "%Y-%m-%d"
HTML_TIME_FORMAT = "%H:%M"

LEFT_OFFSET = 20
TOP_OFFSET = 5
HOR_PIXELS_PER_MIN = 6.0
VER_PIXELS_PER_CHAN = 35
HOR_GAP_PIXELS = 10
VER_GAP_PIXELS = 12
SCREEN_WIDTH_PIXELS = rtv_utils.MINS_IN_DAY * HOR_PIXELS_PER_MIN

DAY_START = datetime.time( 6, 0 )

class TVGuideGenerator( object ):

	def __init__( self, config ):
		self.config = config
		self.startTime = datetime.datetime.today()

	def timesHoursToMinsDiff( self, laterTime, earlierTime ):
		if laterTime.hour > earlierTime.hour:
			return 60 * (laterTime.hour - earlierTime.hour) + (laterTime.minute - earlierTime.minute)
		elif laterTime.hour < earlierTime.hour:
			return 60 * ( laterTime.hour + 24 - earlierTime.hour ) + (laterTime.minute - earlierTime.minute)
		elif laterTime.minute >= earlierTime.minute:
			return laterTime.minute - earlierTime.minute
		else:
			return rtv_utils.MINS_IN_DAY + (laterTime.minute - earlierTime.minute)



	def get_times( self ):

		time_tmpl = rtv_utils.read_file_into_str( self.config.html_time_file )

		ret = ""
		tm = DAY_START
		for i in range( 24 ):

			startMins = self.timesHoursToMinsDiff( tm, DAY_START )
			lengthMins = 60

			x = LEFT_OFFSET + startMins*HOR_PIXELS_PER_MIN
			width = lengthMins*HOR_PIXELS_PER_MIN - HOR_GAP_PIXELS

			repl = time_tmpl.replace( "$time", tm.strftime( HTML_TIME_FORMAT ) ).replace(
				"$left", str( x ) ).replace(
				"$width", str( width ) )
			ret += repl

			if tm.hour == 23:
				tm = tm.replace( hour=0 )
			else:
				tm = tm.replace( hour=tm.hour+1 )

		return ret

	def generate( self ):

		self.favs_and_sels = rtv_selection.read_favs_and_selections(
			self.config )

		self.channel_xmltv2tzap = rtv_propertiesfile.PropertiesFile()
		self.channel_xmltv2tzap.load( self.config.channel_xmltv2tzap_file
			 )

		self.channels_order = OrderedPropertiesFile()
		self.channels_order.load( self.config.channels_order_file )

		rtv_utils.ensure_dir_exists( self.config.tvguide_dir )

		# Hash date -> open file
		self.html_files = {}
		self.times = self.get_times()
		self.channels = ""

		self.single_day_foot = None
		self.single_day_head = None
		( self.single_day_head_tmpl, self.single_day_foot ) = rtv_utils.read_file_into_str(
			self.config.html_single_day_file ).split( "$programmes" )

		self.prog_small_normal = rtv_utils.read_file_into_str(
			self.config.html_programme_small_normal_file )
		self.prog_small_highlight = rtv_utils.read_file_into_str(
			self.config.html_programme_small_highlight_file )
		self.prog_small_remind = rtv_utils.read_file_into_str(
			self.config.html_programme_small_remind_file )
		self.prog_small_record = rtv_utils.read_file_into_str(
			self.config.html_programme_small_record_file )

		self.prog_full_normal = rtv_utils.read_file_into_str(
			self.config.html_programme_full_normal_file )
		self.prog_full_highlight = rtv_utils.read_file_into_str(
			self.config.html_programme_full_highlight_file )
		self.prog_full_remind = rtv_utils.read_file_into_str(
			self.config.html_programme_full_remind_file )
		self.prog_full_record = rtv_utils.read_file_into_str(
			self.config.html_programme_full_record_file )

		self.cur_id = 0
		rtv_utils.parse_xmltv_files( self.config, self.sax_callback )

		for dt in self.html_files.keys():
			fl = self.html_files[dt]
			fl.write( rtv_utils.encode_text( self.single_day_foot ) )
			fl.close()

	def timedeltaToMins( self, timedelta ):
		return ( ( timedelta.days * rtv_utils.SECS_IN_DAY ) + timedelta.seconds ) / 60

	def get_file_for_date( self, date ):
		if date not in self.html_files:
			fn = os.path.join( self.config.tvguide_dir,
				"tv-%s.html" % date.strftime( FILENAME_DATE_FORMAT ) )
			print "Creating file '%s'" % fn
			fl = file( fn, "w" )
			self.html_files[date] = fl

			# We assume that by the time we get to any programmes, we have been given all the channels
			if self.single_day_head == None:
				self.single_day_head = self.single_day_head_tmpl.replace( "$height",
					str( VER_PIXELS_PER_CHAN - VER_GAP_PIXELS ) )
				self.single_day_head = self.single_day_head.replace( "$times", self.times )
				self.single_day_head = self.single_day_head.replace( "$channels", self.channels )

			fl.write( rtv_utils.encode_text( self.single_day_head ) )
		else:
			fl = self.html_files[date]

		return fl


	def get_adjusted_date( self, dt ):
		rdt = dt.date()
		startTimeTime = dt.time()
		if startTimeTime < DAY_START:
			adjStartDate = rdt + datetime.timedelta( - 1 )
		else:
			adjStartDate = rdt
		return adjStartDate

	def sax_callback( self, pi ):

		adjStartDate = self.get_adjusted_date( pi.startTime )
		adjEndDate = self.get_adjusted_date( pi.endTime )

		fl = self.get_file_for_date( adjStartDate )
		self.add_prog_to_file( pi, fl, adjStartDate )

		if adjEndDate != adjStartDate:
			fl2 = self.get_file_for_date( adjEndDate )
			self.add_prog_to_file( pi, fl2, adjEndDate )

		self.cur_id += 1

	def add_prog_to_file( self, pi, fl, dt ):

		dayStart = datetime.datetime.combine( dt, DAY_START )

		extra_style = ""

		startMins = self.timedeltaToMins( pi.startTime - dayStart )
		lengthMins = self.timedeltaToMins( pi.endTime - pi.startTime )
		tmfmt = pi.startTime.time().strftime( HTML_TIME_FORMAT )

		if startMins + lengthMins < 0:
			print "Programme appearing too early?"

		if startMins > rtv_utils.MINS_IN_DAY:
			print "Programme appearing too late?"

		if startMins < 0:
			lengthMins += startMins
			startMins = 0
			extra_style += "border-left-style: none"

		if startMins + lengthMins >= rtv_utils.MINS_IN_DAY:
			lengthMins = rtv_utils.MINS_IN_DAY - startMins
			extra_style += "border-right-style: none"

		if not self.channels_order.has_key( pi.channel ):
			self.channels_order.set_value( pi.channel, pi.channel )
		channelNum = self.channels_order.get_position( pi.channel )

		x = LEFT_OFFSET + startMins*HOR_PIXELS_PER_MIN

		y = TOP_OFFSET + channelNum*VER_PIXELS_PER_CHAN

		width = lengthMins*HOR_PIXELS_PER_MIN - HOR_GAP_PIXELS

		if width > 0:
			matched = False
			for fav in self.favs_and_sels:
				if fav.matches( pi ):
					matched = True
					prog_small_html = self.prog_small_record
					prog_full_html = self.prog_full_record

			if not matched:
				prog_small_html = self.prog_small_normal
				prog_full_html = self.prog_full_normal

			prog_small_html = prog_small_html.replace( "$top", "%d" % y )
			prog_small_html = prog_small_html.replace( "$left", "%d" % x )
			prog_small_html = prog_small_html.replace( "$width", "%d" % width )
			prog_small_html = prog_small_html.replace( "$time", tmfmt )
			prog_small_html = prog_small_html.replace( "$title", pi.title )
			prog_small_html = prog_small_html.replace( "$style", extra_style )
			prog_small_html = prog_small_html.replace( "$id",
				"%d" % self.cur_id )

			fl.write( rtv_utils.encode_text( prog_small_html ) )

			prog_full_html = prog_full_html.replace( "$title", pi.title )
			prog_full_html = prog_full_html.replace( "$time", tmfmt )
			prog_full_html = prog_full_html.replace( "$channel",
				pi.channel )
			prog_full_html = prog_full_html.replace( "$desc",
				pi.description )

			full_fl = file( os.path.join( self.config.tvguide_dir,
				"prog-%d.inc" % self.cur_id ), "w" )
			full_fl.write( rtv_utils.encode_text( prog_full_html ) )
			full_fl.close()



def generate( config ):
	generator = TVGuideGenerator( config )
	generator.generate()

