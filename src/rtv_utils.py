#!/usr/bin/python

import os, select, re, datetime, time, xml.parsers.expat, popen2
import rtv_saxhandler

SECS_IN_DAY = 60*60*24
MINS_IN_DAY = 60*24

def ensure_dir_exists( dr ):
	if os.path.isfile( dr ):
		raise Exception( ( "Can't create config directory '%s' "
			+ "- there is a file already there!" ) % dr )

	if not os.path.isdir( dr ):
		os.makedirs( dr )

ALLOWED_CHARS_RE = re.compile( "[0-9a-zA-Z_-]" )
def prepare_filename( fn ):
	ret = ""
	for ch in fn[:20]:
		if ALLOWED_CHARS_RE.match( ch ):
			ret += ch
		else:
			ret += "_"
	return ret

def run_command( cmd_array ):

	pop = popen2.Popen4( " ".join( cmd_array ) )

	ret_val = pop.wait()

	#print "Return value = '%s'" % ret_val

	return ret_val == 0

def run_command_write_output( cmd_array, output_filename ):

	cmd = " ".join( cmd_array )
	cmd += " > "
	cmd += output_filename

	#print "Running command " + cmd

	ret_val = os.system( cmd )

	#print "Return value = '%s'" % ret_val

	return ret_val == 0

def run_command_feed_input( cmd_array, input_string ):

	#print "Running command " + str( cmd_array )
	(i, oe) = os.popen4( cmd_array )

	#print "Feeding input " + input_string

	i.write( input_string )
	i.close()

	ret = []
	for ln in oe.readlines():
		ret.append( ln )
		#print "Output: " + ln,

	return ret

def parse_xmltv_files( config, sax_callback ):

	parser = xml.parsers.expat.ParserCreate()
	handler = rtv_saxhandler.SaxHandler( sax_callback )
	parser.StartElementHandler = handler.startElement
	parser.EndElementHandler = handler.endElement
	parser.CharacterDataHandler = handler.characters

	done = False
	for fn in os.listdir( config.xmltv_files_dir ):
		if fn[-6:] == ".xmltv":
			done = True

			full_fn = os.path.join( config.xmltv_files_dir, fn )
			#print "Parsing '%s'" % full_fn
			fl = file( full_fn, "r" )
			parser.ParseFile( fl )
			fl.close()

	if not done:
		print ( "Unable to parse xmltv listings - no files found"
			+ " in directory '%s'." % config.xmltv_files_dir )


def parse_datetime( stringdatetime, format ):
	return datetime.datetime( *time.strptime( stringdatetime, format )[0:6] )

def read_file_into_str( filename ):
	fl = file( filename, "r" )
	ans = fl.read()
	fl.close()

	return ans

def encode_text( text ):
	return unicode( text ).encode( 'ascii', 'ignore' )

