#!/usr/bin/python

import re
import rtv_propertiesfile, rtv_programmeinfo

class Favourite(rtv_propertiesfile.PropertiesFile):

	def __init__( self ):
		self.title_re = None
		self.channel = None
		self.deleteAfterDays = None
		self.priority = None

		self._title_re = None

	def matches( self, pi ):

		if self.title_re == None:
			return False	# This favourite is empty
		else:
			if self._title_re == None:
				self._title_re = re.compile( self.title_re + "$" )

			return ( self._title_re.match( pi.title ) and
				( self.channel == None or self.channel == pi.channel ) )
			# TODO: other things to match on e.g. time, categories

