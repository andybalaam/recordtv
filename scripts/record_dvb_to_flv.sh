#!/bin/bash

# Example:

# ./record_dvb_to_flv.sh "BBC ONE" eastenders 1800 \
#     "../scheduled_recordings/eastenders.rtvinfo"

# ... will record into a file called eastenders.flv

echo '$ killall tzap'
killall tzap

if [ "$?" == "0" ]; then
{
    echo '$ sleep 10s'
    sleep 10s
}; fi

echo '$ tzap -r "'$1'" > /dev/null &'
tzap -r "$1" > /dev/null &

echo '$ sleep 1s'
sleep 1s

echo 'mv "'$4'" "'$2'.rtvinfo"'
mv "$4" "$2.rtvinfo"

echo '$ cat /dev/dvb/adapter0/dvr0 > "'$2'.ts" &'
cat /dev/dvb/adapter0/dvr0 > "$2.ts" &

echo '$ sleep 5s'
sleep 5s

SIZE=400x240

echo '$ /opt/ffmpeg/bin/ffmpeg -r 25 -t '$3' -re -i "'$2'.ts" -b 1000k -async 1000 -ar 44100 -s '$SIZE' -f flv "'$2'.flv"'
/opt/ffmpeg/bin/ffmpeg -r 25 -t $3 -re -i "$2.ts" -b 1000k -async 1000 -ar 44100 -s $SIZE -f flv "$2.flv"

echo "ffmpeg returned '"$?"'"

echo '$ kill %cat'
kill %cat

echo '$ kill %tzap'
kill %tzap

echo '$ sleep 1s'
sleep 1s

echo '$ rm "'$2'.ts"'
rm "$2.ts"

