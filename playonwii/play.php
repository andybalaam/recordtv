<?php

$curfiletitle = htmlspecialchars( $_GET['title'], ENT_QUOTES );
$curfilenum = $_GET['file'];
$large = $_GET['large'] == "1";

if( $large )
{
    $width = 1000;
    $height = 565;
}
else
{
    $width = 400;
    $height = 240;
}

# This is a list of all the files in the directory
$filenames = array();
$handle = opendir( '/home/andy/.recordtv/recorded_programmes' );
if( $handle )
{
    while( ( $filename = readdir( $handle ) ) )
    {
        $filenames[] = $filename;
    }
}

$num = 0;
rsort( $filenames );

foreach( $filenames as $filename )
{
    if( preg_match( '/^(.*)\\.flv$/', $filename, $matches ) )
    {
        if( $num == $curfilenum )
        {
            $curfile = $filename;
            break;
        }
        $num++;
    }
}

?>

<html>

<head>
    <title><?php print "$curfiletitle" ?></title>
    <script type="text/javascript" src="swfobject.js"></script>
</head>

<body style="background-color: black; color: white">
<table width='100%' height='100%'><tr valign='middle'><td align='center'>

    <p id='player1'>No flash player!</p>
    <script type='text/javascript'>
        var s1 = new SWFObject('flvplayer.swf','single','<?php print $width; ?>','<?php print $height; ?>','7');
<?php
    if( $large )
    {
        print "s1.addVariable('overstretch','true');";
    }
    else
    {
        print "s1.addVariable('overstretch','none');";
    }
?>
        s1.addParam('allowfullscreen','true');
        s1.addVariable('file','<?php print "$curfile" ?>');
        s1.addVariable('showvolume','false');
        s1.addVariable('autostart','true');
        s1.addVariable('displayheight','<?php print $height; ?>');
        s1.addVariable('largecontrols','true');
        s1.addVariable('overstretch','none');
        s1.write('player1');
    </script>

</td></tr><table>


