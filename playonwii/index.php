<?php

function pretty_date( $date )
{
    global $num_to_month;

    $tm = mktime(
        (int)( substr( $date, 11, 2 ) ),
        (int)( substr( $date, 14, 2 ) ),
        0,
        (int)( substr( $date, 5, 2 ) ),
        (int)( substr( $date, 8, 2 ) ),
        (int)( substr( $date, 0, 4 ) ) );

    return date( "D d M, H:i", $tm );
}

function get_info_from_file( $fn )
{
    $title = "";
    $sub_title = "";
    $description = "";
    $date = "";
    $channel = "";

    $handle = fopen( $fn, "r" );

    if( $handle )
    {
        while ( !feof( $handle ) )
        {
            $line = fgets( $handle );
            $line = substr( $line, 0, -1 );

            list( $k, $v ) = split( "=", $line, 2 );

            switch( $k )
            {
                case "title":
                {
                    $title = $v;
                    break;
                }
                case "sub_title":
                {
                    $sub_title = $v;
                    break;
                }
                case "description":
                {
                    $description = $v;
                    break;
                }
                case "startTime":
                {
                    $date = $v;
                    break;
                }
                case "channel_pretty":
                {
                    $channel = $v;
                    break;
                }
            }
        }

        fclose( $handle );
    }

    return array( $title, $date, $channel, $sub_title, $description );
}

function get_info_from_filename( $fn )
{
    if( preg_match( '/^(.*?)-(.*)\\..*$/', $fn, $matches ) )
    {
        $title = $matches[1];
        $date = $matches[2];
    }
    else
    {
        $title = substr( $fn, 0, strlen( $fn ) - 4 );
        $date = "";
    }

    return array( $title, $date );
}

// This is a hash filename -> nothing of all files in the directory
$filenames = array();
$handle = opendir( '/home/andy/.recordtv/recorded_programmes' );
if( $handle )
{
    while( ( $filename = readdir( $handle ) ) )
    {
        $filenames[$filename] = '';
    }
}

// This is a hash title->array( array( filenumber, date, filename, channel ) )
$titles = array();
$num = 0;

$sorted_fns = array_keys( $filenames );
rsort( $sorted_fns );

foreach( $sorted_fns as $filename )
{

    if( preg_match( '/^(.*)\\.flv$/', $filename, $matches ) )
    {

        $infofn = $matches[1] . ".rtvinfo";

        if( array_key_exists( $infofn, $filenames ) )
        {
            list( $title, $date, $channel, $sub_title, $description ) =
                get_info_from_file( $infofn );
        }
        else
        {
            list( $title, $date ) = get_info_from_filename( $filename );
            $channel = "";
            $sub_title = "";
        }
        $titles[$title][] = array( $num, $date, $channel, $sub_title,
            $description );
        $num++;
    }
}

closedir($handle);

?>

<html>

<head>
    <title>Recorded programmes</title>
    <style type="text/css">
        body {
            font-family: verdana, sans-serif;
            text-align: center;
        }
        a {
            text-decoration: none;
            color: black;
        }
        a:hover {
            color: red;
        }
        span.smalltime {
            font-size: smaller;
            color: gray;
        }
        span.smalltime:hover {
            color: red;
        }
        th {
            font-size: x-large;
            font-weight: normal;
            color: blue;
        }
        td.dates {
            font-size: small;
        }
    </style>
</head>

<body>
<h1>Recorded programmes</h1>

<center>
<?php
print "<table cellpadding='2'>\n";
        ksort( $titles );
        foreach( $titles as $title => $arr )
        {
            list( $num, $date, $channel, $sub_title ) = $arr[0];
            print "<tr><th style='border-style: solid none none none; border-width: 1px; border-color: black;' width='50%' align='right' valign='top'>$title</th><td width='50%' class='dates'>";

            //<a class='tiny' href='play.php?file=$num&large=1&title=$title'>(Large)</a>

            foreach( $arr as $lst )
            {
                list( $num, $date, $channel, $sub_title, $description ) = $lst;
                print "<a href='play.php?file=$num&title=$title'";

                print " title='$description'";

                print ">";

                if( $sub_title )
                {
                    print $sub_title . " <span class='smalltime'>(";
                }

                print pretty_date( $date );

                if( $channel )
                {
                    print " on $channel";
                }

                if( $sub_title )
                {
                    print ")</span>";
                }

                print "</a><br />";

                //<a class='tiny' href='play.php?file=$num&large=1&title=$title'>(Large)</a><br />";
            }
            print "</td></tr>\n";
            print "<tr><th></th><td><br /></td></tr>";
        }
        print "</table>\n";
?>
</center>

</body>

</html>

